extends TextureButton

var press_duration = 0


func attack():
	var force = min(1.0, press_duration)
	Throwables.create_random(force)
	press_duration = 0
	$Timer.stop()


func _on_Timer_timeout():
	press_duration += 0.1
	

func _on_AttackButton_button_down():
	$Timer.start()
