extends Node2D

func create_random(force):
	var main_scene = get_node("/root/Main")

	var random_index = randi() % get_child_count()
	var throwable = get_child(random_index).duplicate() as RigidBody2D
	throwable.sleeping = false
	main_scene.add_child(throwable)

	var impulse = Vector2(70 * throwable.gravity_scale * force, 0)

	var princess = main_scene.get_node("Princess")
	throwable.global_position = princess.global_position
	throwable.global_position.x += 40
	throwable.global_position.y -= 40
	
#	throwable.apply_impulse(princess.global_position, impulse)
	throwable.apply_central_impulse(impulse)
