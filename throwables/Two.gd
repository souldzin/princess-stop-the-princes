extends RigidBody2D

func _on_body_shape_entered(_body_id, body, _body_shape, _local_shape):
	if ($Timer.is_stopped()):
		if(body.name == "GroundNode"):
			self.sleeping = true
			$Timer.start()
			self.collision_layer = 0
			self.collision_mask = 1

func _on_Timer_timeout():
	queue_free()
